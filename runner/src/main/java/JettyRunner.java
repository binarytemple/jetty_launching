import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebInfConfiguration;
import org.eclipse.jetty.webapp.WebXmlConfiguration;

public class JettyRunner {
    private static final Server SERVER = new Server();

    public static void main(String[] args) throws Exception {
        new JettyRunner().init();
    }

    private void init() throws Exception {

        Log.setLog(new org.eclipse.jetty.util.log.Slf4jLog());

        String webapp = this.getClass().getResource("webcontent").getPath();

        WebAppContext app = new WebAppContext();
        app.setContextPath("/");
        app.setWar(webapp);
        // Avoid the taglib configuration because its a PITA if you don't have a
        // net connection
        app.setConfigurationClasses(new String[]{
                WebInfConfiguration.class.getName(),
                WebXmlConfiguration.class.getName()});

        app.setParentLoaderPriority(true);

        // We explicitly use the SocketConnector because the
        // SelectChannelConnector locks files
        Connector connector = new SocketConnector();
        connector.setPort(Integer.parseInt(System.getProperty("jetty.port",
                "6666")));
        connector.setMaxIdleTime(60000);

        Server s = new Server();

        s.setConnectors(new Connector[]{connector});
        s.setHandler(app);
        s.setAttribute("org.mortbay.jetty.Request.maxFormContentSize", 0);
        s.setStopAtShutdown(true);

        try {
            s.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}