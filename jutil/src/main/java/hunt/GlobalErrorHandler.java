package hunt;

/**
 * This class eats exceptions, globabally overriding the normal 404 response. 
 * @author bryan
 */
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;
import org.eclipse.jetty.servlet.ErrorPageErrorHandler;

public class GlobalErrorHandler extends ErrorPageErrorHandler {

	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		int status = ((Response) response).getStatus();
		if (status >= 400 || status <= 599) {
			response.setContentLength(0);
			return;
		}
	}
}